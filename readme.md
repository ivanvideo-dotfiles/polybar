# Description
Polybar config with the gruvbox theme.

# Dependencies
* [polybar](https://github.com/polybar/polybar) obviously
* [tiramisu](https://github.com/Sweets/tiramisu) for notifications
* [ttf-nerd-fonts-symbols](https://github.com/ryanoasis/nerd-fonts) for symbols
* [pacman-contrib](https://gitlab.archlinux.org/pacman/pacman-contrib) for update checking (pacman)
* [pulseaudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) for volume module

# Usage
Run the launch.sh script to start polybar.

## Modules
Some modules are already available in the modules directory, but additional ones may be added. To add them to your bar, edit the main config file.

## Scripts
Some modules may require special scripts in order to function. These may be added in the scripts directory and referenced in the module.

# Disclaimer
The original author of the polybar config is Trollwut (trollwut.sexy). This is a modified version of that config to suit my personal i3wm setup. Feel free to use and modify further.